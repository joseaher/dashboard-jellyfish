# Fornax Morphologuical analysis

This repository contains a dashboard for a morphological analysis
of Fornax cluster. The data was generate using [`astromorph`](https://gitlab.com/joseaher/astromorphlib) package.

[See what it looks like here.](https://joseaher.gitlab.io/fornax-morphology/)

---

## Running in your PC

An enhance and faster version of this Dashboard can be used by cloning this repository:

    git clone git@gitlab.com:joseaher/fornax-morphology.git

 go to fornax-morphology directory and execute the command:

    bokeh serve --show public


See requirement file to see the versions of
the packages to run the app properly.
